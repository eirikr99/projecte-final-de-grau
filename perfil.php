<?php

session_start();

require 'database.php';

if (isset($_SESSION['user_id'])) {
    $id = $_SESSION['user_id'];
    $records = $conn->prepare('SELECT * FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = null;

    if (!empty($results)) {
        $user = $results;
    }

    $recordsCount = $conn->prepare("SELECT COUNT(id_publicacion) FROM publicaciones WHERE usuario = $id");
    $recordsCount->execute();
    $resultadoCount = $recordsCount->fetch(PDO::FETCH_ASSOC);
    $count = null;

    if (!empty($resultadoCount)) {
        $count = $resultadoCount;
    }
} else {
    header("Location: /Projecte/projecte-final-de-grau");
} //Guardar datos del usuario actual en la sesión

if (isset($_SESSION['user_id'])) {
    if (isset($_POST['submit'])) {
        $id = $_SESSION['user_id'];
        $sql = "UPDATE users SET user = :userNew, email = :emailNew, direccion = :direccionNew, aniversario = :dateNew WHERE id = $id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':userNew', $_POST['user']);
        $stmt->bindParam(':emailNew', $_POST['email']);
        $stmt->bindParam(':direccionNew', $_POST['direccion']);
        $stmt->bindParam(':dateNew', $_POST['cumpleanos']);

        if ($stmt->execute()) {
            header("Location: /Projecte/projecte-final-de-grau");
        } else {
            $message = 'Sorry there must have been an issue creating your account';
        }
    }
} //Guardar datos del usuario actual en la sesión para mostrarlos en el perfil

if (!empty($_POST['query'])) {

  $busqueda = $_POST['query'];
  $records = $conn->prepare("SELECT id_publicacion, titulo, resumen, usuario, fecha FROM publicaciones WHERE titulo LIKE '$busqueda'") ;
  $records->execute();
  $resultado = $records->fetch(PDO::FETCH_ASSOC);
      
  if($resultado['id_publicacion'] != null){
    header("Location: view.php?id=".$resultado['id_publicacion']);
  } else {
    header("Location: index.php");
  }
  
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Principal</title>
  
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

  <?php
  $entradaBuscada = "";
    echo "<script type='text/javascript'>
     $(document).ready(function() {
      $('.modal').modal();
      $('.sidenav').sidenav();
      $('.dropdown-trigger').dropdown();
       $('#buscarBt').click(function(){
             $.ajax({
               type: 'POST',
               url: 'buscar.php',
               data: {'titol': $('#buscar').val()},
               success: function(data)
               { 
                 if (data !== 'empty'){
                  window.location.replace('index.php');
                } else {
                  M.toast({html: 'No se han encontrado resultados', classes: 'rounded'});
                }
                 
               }           
           });
          
         });
         
       });


          
       </script>";

       if( $_SESSION['dato'] !== ""){
        if ($_SESSION['buscados'] !== ""){
          $entradaBuscada = $_SESSION['buscados'];
          if($entradaBuscada == "empty"){
            $entradaBuscada = " ";
          }
         } 
       } else {
        $entradaBuscada = " ";
       }

       
       
?>

</head>
<style>
 body {
     display: flex;
     min-height: 100vh;
     flex-direction: column;
 }
 main {
     flex: 1 0 auto;
 }
 #login {
  background-color: white;
  border-radius: 10px;
}
#login_title {
  background-color: #4e342e !important;
  margin-top: 0px;
}
 </style>
<body class="orange accent-2">
<div class="navbar-fixed">
<nav class="brown darken-4">
      <div class="nav-wrapper">
      <a href="index.php" class="brand-logo" style="margin-left: 12%" id="logo">E-Story</a>
        <a href="#" class="sidenav-trigger" data-target="responsive-nav">
          <i class="material-icons">menu</i>
        </a>
        <ul class="right hide-on-med-and-down">
          <li>
              <input type="text" id="buscar" class="autocomplete" style="background-color: white; " name="query">      
          </li>
          <li><a class="brown darken-2" style="color:white;" id="buscarBt">Buscar</a></li>
          <ul id="dropdown1" class="dropdown-content">
          <li><a href="#search" class="modal-trigger brown darken-2" style="color:white;">Buscador Avanzado</a></li>
          <?php if (!empty($user)): ?>
          <li><a href="new_post.php" class="brown darken-2" style="color:white;">Publicar</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <li><a href="#login" class="modal-trigger brown darken-2" style="color:white;">Entrar</a></li>
          <li><a href="signup.php" class="brown darken-2" style="color:white;">Registrate</a></li>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
          </ul>
          <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Acciones<i class="material-icons right">arrow_drop_down</i></a></li>
          
      </div>
    </nav>
  </div>

  <ul class="sidenav" id="responsive-nav">
    <?php if (!empty($user)): ?>
    <li><a href="new_post.php" class="brown darken-2" style="color:white;">Publicar</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <li><a href="#login" class="modal-trigger brown darken-2" style="color:white;">Entrar</a></li>
          <li><a href="signup.php" class="brown darken-2" style="color:white;">Registrate</a></li>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
      </ul>

    <div id="cuerpo">   
    <br>
    <br>

      <div class="row container">
      <div class="card-panel teal white">
        <h3 class="center-align">PERFIL</h3>       
        <?php
            if (!empty($count)) {
                    print("
                    <b>Publicaciones: </b>".$count['COUNT(id_publicacion)']."
                    <form action='perfil.php' method='POST' idea>
                    <div class='row'>
                    <br>
                      <div class='input-field col s12'>
                        <input name='user' type='text' placeholder='Usuario *' id='name' class='validate' value=".$user['user']." >
                        <label for='name'>Nombre</label>
                        <span class='helper-text'></span>
                      </div>
                      <div class='input-field col s12'>
                        <input name='email' type='email' placeholder='Correo *' id='email' class='validate' value=".$user['email']." >
                        <label for='email'>Correo</label>
                        <span class='helper-text'></span>
                      </div>
                      <div class='input-field col s12'>
                        <input type='text' name='direccion' placeholder='Direccion' id='direccion' value=".$user['direccion'].">
                        <label for='direccion'>Dirección</label>
                      </div> 
                      <div class='input-field col s12'>
                        <input type='date' name='cumpleanos' placeholder='Cumpleaños' id='cumple' value=".$user['aniversario'].">
                        <label for='cumple'>Aniversario</label>
                      </div> 
                      <div class='input-field col s12'>          
                        <input type='submit' name='submit' class='btn btn-large brown darken-3' value='Submit'>
                      </div>
                    </div>
                  </form>
                          ");
            }
        ?>
        </div>
      </div>
   </div>

   <footer class="page-footer brown darken-4 fixed">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">¿Qué es E-Story?</h5>
                <p class="grey-text text-lighten-4">
                E-Story, nace de la necesidad de tener un sitio en el que poder compartir con otras personas opiniones, información y más cosas sobre nuestra pasión común, la historia.
                </p>
                <p class="grey-text text-lighten-4">
                Aquí podrás encontrar todo lo que necesitas, además de satisfacer tu curiosidad o ayudarte en cualquier proyecto que necesite de información, a la vez que discutes
                con otros usuarios. 
                </p>             
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Efemérides, ¿Qué pasó hoy?</h5>
                <br>
                <?php

                $efem = $conn->prepare('SELECT * FROM efemerides WHERE dia = '.date('d').' AND mes = '.date('m').'');
                $efem->execute();            
                $resultadoEfem = $efem->fetch(PDO::FETCH_ASSOC);
                print($resultadoEfem['texto']);?>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © Copyright 2018-2019 Erik Cañizares
            <a class="grey-text text-lighten-4 right" href="signup.php"><b>¡Regístrate en menos de 5 minutos!</b></a>
            </div>
          </div>
        </footer>

      <div id="login" class="modal card">
        <h5 class="modal-close">&#10005;</h5>
        <div class="card-action teal lighten-1 white-text" id="login_title">
          <h3 class="center">Logueate</h3>
        </div>
        <div class="modal-content center">
          <br>
          <form action="index.php" id="login" method="post">
            <div class="input-field">
              <i class="material-icons prefix">person</i>
              <input type="text" id="correo" name="email">
              <label for="name">Correo</label>
            </div>
            <br>
            <div class="input-field">
              <i class="material-icons prefix">lock</i>
              <input type="password" id="pass" name="password">
              <label for="pass">Contraseña</label>
            </div>
            <br>
            <div class="">
              <a href="signup.php"><h4>Registrate</h4></a>
            </div>
            <input type="submit" value="Entra" class="btn btn-large brown darken-3">
          </form>
        </div>

        <div id="search" class="modal card">
        <h5 class="modal-close">&#10005;</h5>
        <div class="card-action teal lighten-1 white-text" id="search_title">
          <h3 class="center">Buscador Avançat</h3>
        </div>
        <div class="modal-content center">
          <br>
            <div class="input-field">
              <i class="material-icons prefix">person</i>
              <input type="text" id="correo" name="email">
              <label for="name">Correo</label>
            </div>
            <br>
            <div class="input-field">
              <i class="material-icons prefix">lock</i>
              <input type="password" id="pass" name="password">
              <label for="pass">Contraseña</label>
            </div>
            <br>
            <div class="">
              <a href="signup.php"><h4>Registrate</h4></a>
            </div>
            <li><a class="brown darken-2" style="color:white;" id="buscarAv">Buscar</a></li>

        </div>
      
      
      </div>
    
</div>

</body>

</html>
