<?php 

session_start();

require 'database.php';

$id_publ = $_GET['id'];
$id_user = $_GET['id'];


if (isset($_SESSION['user_id'])) {
  
    $id = $_SESSION['user_id'];
    $records = $conn->prepare('INSERT INTO comentarios (id_user, id_publicacion, texto, fecha) VALUES (:user, :publ, :texto, :fecha)');
    $records->bindParam(':user', $id_user);
    $records->bindParam(':publ', $id_publ);
    $stmt->bindParam(':fecha', date("Y-m-d H:i:s"));
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);


} else {
    header("Location: ". $_SERVER['HTTP_REFERER']);
}
?>