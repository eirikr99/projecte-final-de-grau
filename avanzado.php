<?php

session_start();

require 'database.php';

if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $records = $conn->prepare('SELECT id, user, email, pass, direccion, aniversario FROM users WHERE email = :email');
    $records->bindParam(':email', $_POST['email']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $message = '';

    if (!empty($results) && password_verify($_POST['password'], $results['pass'])) {
        $_SESSION['user_id'] = $results['id'];
        header("Location: /Projecte/projecte-final-de-grau");
        $message = 'Estás dentro';
    } else {
        $message = 'Sorry, those credentials do not match';
    }
} //Loguearse

if (isset($_SESSION['user_id'])) {
    $id = $_SESSION['user_id'];
    $records = $conn->prepare('SELECT * FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = null;

    if (!empty($results)) {
        $user = $results;
    }

    $recordsCount = $conn->prepare("SELECT COUNT(id_publicacion) FROM publicaciones WHERE usuario = $id");
    $recordsCount->execute();
    $resultadoCount = $recordsCount->fetch(PDO::FETCH_ASSOC);
    $count = null;

    if (!empty($resultadoCount)) {
        $count = $resultadoCount;
    }
} //Guardar datos del usuario actual en la sesión

if (isset($_SESSION['user_id'])) {
    if (!empty($_POST['userNew']) && !empty($_POST['emailNew']) && !empty($_POST['direccionNew']) && !empty($_POST['dateNew'])) {
        $id = $_SESSION['user_id'];
        $sql = "UPDATE users SET user = :userNew, email = :emailNew, direccion = :direccionNew, aniversario = :dateNew WHERE id = $id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':userNew', $_POST['userNew']);
        $stmt->bindParam(':emailNew', $_POST['emailNew']);
        $stmt->bindParam(':direccionNew', $_POST['direccionNew']);
        $stmt->bindParam(':dateNew', $_POST['dateNew']);

        if ($stmt->execute()) {
            header("Location: /Projecte/projecte-final-de-grau");
            $message = 'Successfully created new user';
        } else {
            $message = 'Sorry there must have been an issue creating your account';
        }
    }
} //Guardar datos del usuario actual en la sesión para mostrarlos en el perfil

$entradas = array();
$entradasBusqueda = array();
$busqueda = "";

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Principal</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <?php
  $entradaBuscada = "";
    echo "<script type='text/javascript'>
     $(document).ready(function() {
      $('.modal').modal();
      $('select').formSelect();
      $('.sidenav').sidenav();
      $('.dropdown-trigger').dropdown();
      $('#logo').click(function(){
        location.reload();
      });
      $('#buscarBt').click(function(){
        $.ajax({
          type: 'POST',
          url: 'buscar.php',
          data: {'titol': $('#buscar').val()},
          success: function(data)
          { 
            if (data !== 'empty'){
                window.location.replace('index.php'); 
           } else {
             M.toast({html: 'No se han encontrado resultados', classes: 'rounded'});
           }
            
          }           
      });
    });
       $('#buscarAv').click(function(){
           if($('#any').val() == ''){
        }
             $.ajax({
               type: 'POST',
               url: 'busquedaAvanzada.php',
               data: {
                   'categoria': $('select[name=categorias]').val(),
                   'pais': $('#pais').val(),
                   'any': $('#any').val()
                },
               success: function(data)
               { 
                 if (data !== 'empty'){
                    window.location.replace('avanzado.php'); 
                    console.log(data);
                } else {
                  M.toast({html: 'No se han encontrado resultados', classes: 'rounded'});
                }
                 
               }           
           });
          
         });
         
       });
     
       </script>";

      
       if(isset($_SESSION['dato'])) 
       if($_SESSION['dato'] !== ""){
        if ($_SESSION['buscados'] !== ""){
          $entradaBuscada = $_SESSION['buscados'];
          if($entradaBuscada == "empty"){
            $entradaBuscada = " ";
          }
         } 
       } else {
        $entradaBuscada = " ";
       }
    
?>

</head>

<style>
 body {
     display: flex;
     min-height: 100vh;
     flex-direction: column;
 }
 main {
     flex: 1 0 auto;
 }
 #login {
  background-color: white;
  border-radius: 10px;
}
#login_title {
  background-color: #4e342e !important;
  margin-top: 0px;
}
 </style>
 
<body class="orange accent-2">
<div class="navbar-fixed">
    <nav class="brown darken-4">
      <div class="nav-wrapper">
        <a href="index.php" class="brand-logo" style="margin-left: 12%" id="logo">E-Story</a>
        <a href="#" class="sidenav-trigger" data-target="responsive-nav">
          <i class="material-icons">menu</i>
        </a>
        <ul class="right hide-on-med-and-down">
          <li>
              <input type="text" id="buscar" class="autocomplete" style="background-color: white; " name="query">      
          </li>
          <li><a class="brown darken-2" style="color:white;" id="buscarBt">Buscar</a></li>
          <ul id="dropdown1" class="dropdown-content">
          <?php if (!empty($user)): ?>
          <li><a href="new_post.php" class="brown darken-2" style="color:white;">Publicar</a></li>
          <li><a href="perfil.php" class="brown darken-2" style="color:white;">Perfil</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <li><a href="#login" class="modal-trigger brown darken-2" style="color:white;">Entrar</a></li>
          <li><a href="signup.php" class="brown darken-2" style="color:white;">Registrate</a></li>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
          </ul>
          <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Acciones<i class="material-icons right">arrow_drop_down</i></a></li>
          
      </div>
    </nav>
  </div>

    <ul class="sidenav" id="responsive-nav">
    <?php if (!empty($user)): ?>
    <li><a href="new_post.php" class="brown darken-2" style="color:white;">Publicar</a></li>
          <li><a href="perfil.php" class="brown darken-2" style="color:white;">Perfil</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <li><a href="#login" class="modal-trigger brown darken-2" style="color:white;">Entrar</a></li>
          <li><a href="signup.php" class="brown darken-2" style="color:white;">Registrate</a></li>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
      </ul>

<main>
<div id="cuerpo">
<br> 
    <div class="row">
        <div class="col s12 m4 l2"></div>
        <div class="col s12 m4 l8 center-align">
            <div class="card-panel teal white">
                <h3><b>BUSCADOR AVANZADO</b></h3>
                <br>
                <div class="row">
                    <div class="col s12 m4 l4">
                        <div class="input-field col s12">
                            <select name="categorias" name="categorias">
                                <option value="" disabled selected>Categorías</option>
                                <option value="prehistoria">Prehistoria</option>
                                <option value="edad_antigua">Edad Antigua</option>
                                <option value="edad_media">Edad Media</option>
                                <option value="edad_moderna">Edad Moderna</option>
                                <option value="edad_contemporanea">Edad Contemporanea</option>
                            </select>
                            <label>Escoge la categoría</label>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="input-field col s12">
                            <input id="pais" name="pais" type="text" placeholder="País">
                            <span class="helper-text">País</span>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="input-field col s12">
                            <input id="any" name="any" type="text" placeholder="Año">
                            <span class="helper-text">Año</span>
                        </div>
                    </div>
                    <div class="col s12 m4 l4"></div>
                    <div class="col s12 m4 l4">
                        <div class="input-field col s12">
                            
                            <a class="brown darken-2 btn" style="color:white;" id="buscarAv">Buscar</a>
                        </div>                   
                </div>
            </div>
        </div>
        <div class="col s12 m4 l2"></div>
        <div class="row">
        <div class="col s12 m4 l2"></div>
        <div class="col s12 m4 l8 center-align">
        </div>
        <div class="col s12 m4 l2"></div>
        <div id="divPubl">
        <?php
            if($entradaBuscada !== " "){
            $img = "";
            if(isset($_SESSION['dato'])) {
                foreach ($entradaBuscada as $entrada) {

                if ($entrada["categoria"] == "prehistoria"){
                    $img = "assets/images/prehistoria.jpg";
                }

                if ($entrada["categoria"] == "edad_antigua"){
                    $img = "assets/images/edad-antigua.jpg";
                }
                
                if ($entrada["categoria"] == "edad_media"){
                    $img = "assets/images/edad-media.jpg";
                }

                if ($entrada["categoria"] == "edad_moderna"){
                    $img = "assets/images/edad-moderna.jpg";
                }

                if ($entrada["categoria"] == "edad_contemporanea"){
                    $img = "assets/images/edad-contemporanea.jpg";
                }

                    print("<div class='col s6'>
                            <div class='card small horizontal hoverable'>
                            <div class='card-image'>
                                <img src='".$img."'>
                                </div>
                                <div class='card-content'>
                                <h6> <b>".$entrada['titulo']. " / ".$entrada['any']." / ".$entrada['pais']."</b></h6>
                                <p>".$entrada['resumen']."</p>
                                </div>
                                <div class='card-action'>
                                <a href='view.php?id=".$entrada['id_publicacion']."'><b>Entra</b></a>
                                </div>
                            </div>
                            </div>
                            ");
                }
                $entradaBuscada = " ";
                $_SESSION['buscados'] = 'empty';
            } 
                
            } 
                
                
            ?>
            </div>
      </div>
      </main>

      <footer class="page-footer brown darken-4 fixed">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">¿Qué es E-Story?</h5>
                <p class="grey-text text-lighten-4">
                E-Story, nace de la necesidad de tener un sitio en el que poder compartir con otras personas opiniones, información y más cosas sobre nuestra pasión común, la historia.
                </p>
                <p class="grey-text text-lighten-4">
                Aquí podrás encontrar todo lo que necesitas, además de satisfacer tu curiosidad o ayudarte en cualquier proyecto que necesite de información, a la vez que discutes
                con otros usuarios. 
                </p>             
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Efemérides, ¿Qué pasó hoy?</h5>
                <br>
                <?php

                $efem = $conn->prepare('SELECT * FROM efemerides WHERE dia = '.date('d').' AND mes = '.date('m').'');
                $efem->execute();            
                $resultadoEfem = $efem->fetch(PDO::FETCH_ASSOC);
                print($resultadoEfem['texto']);?>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © Copyright 2018-2019 Erik Cañizares
            <a class="grey-text text-lighten-4 right" href="signup.php"><b>¡Regístrate en menos de 5 minutos!</b></a>
            </div>
          </div>
        </footer>

      <div id="login" class="modal card">
        <h5 class="modal-close"></h5>
        <div class="card-action teal lighten-1 white-text" id="login_title">
          <h3 class="center">Logueate</h3>
        </div>
        <div class="modal-content center">
          <br>
          <form action="index.php" id="login" method="post">
            <div class="input-field">
              <i class="material-icons prefix">person</i>
              <input type="text" id="correo" name="email">
              <label for="name">Correo</label>
            </div>
            <br>
            <div class="input-field">
              <i class="material-icons prefix">lock</i>
              <input type="password" id="pass" name="password">
              <label for="pass">Contraseña</label>
            </div>
            <br>
            <div class="">
              <a href="signup.php"><h4>Registrate</h4></a>
            </div>
            <input type="submit" value="Entra" class="btn btn-large brown darken-3">
          </form>
        </div>
        
      </div>
    
</div>

</body>

</html>