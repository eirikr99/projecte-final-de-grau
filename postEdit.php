<?php

session_start();

require 'database.php';


$id_publ = $_GET['id'];

if (!isset($_SESSION['user_id']) ) {
    header("Location: /Projecte/projecte-final-de-grau");
    if($_SESSION['user_id'] != $id_publ ){
        header("Location: view.php?id=".$id_publ."");
    }
}

if (!empty($_POST['texto'])) {
    $sql = "UPDATE publicaciones SET titulo = :titulo, texto = :texto, resumen = :resumen WHERE id_publicacion = $id_publ";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':titulo', $_POST['title']);
    $stmt->bindParam(':texto', $_POST['texto']);
    $stmt->bindParam(':resumen', $_POST['resumen']);

    if ($stmt->execute()) {
        header("Location: view.php?id=".$id_publ."");
        var_dump($_POST['resumen']);
    } else {
        var_dump($id_publ);
    }
}

$records = $conn->prepare("SELECT * FROM publicaciones WHERE id_publicacion = $id_publ");
    $records->execute();
    $resultado = $records->fetch(PDO::FETCH_ASSOC);
    $publicacion = null;

    if (!empty($resultado)) {
      $publicacion = $resultado;
      $id_autor = $publicacion['usuario'];
      /*$records2 = $conn->prepare("SELECT * FROM users WHERE id = $id_autor");
      $records2->execute();
      $resultado2 = $records2->fetch(PDO::FETCH_ASSOC);
      $autor = null;

      if (!empty($resultado2)) {
          $autor = $resultado2;
      }*/
}

if (!empty($_POST['query'])) {

  $busqueda = $_POST['query'];
  $records = $conn->prepare("SELECT id_publicacion, titulo, resumen, usuario, fecha FROM publicaciones WHERE titulo LIKE '$busqueda'") ;
  $records->execute();
  $resultado = $records->fetch(PDO::FETCH_ASSOC);
      
  if($resultado['id_publicacion'] != null){
    header("Location: view.php?id=".$resultado['id_publicacion']);
  } else {
    header("Location: index.php");
  }
  
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

    <script type="text/javascript">
      $('#select').formSelect();
      $('.modal').modal();
      $('.sidenav').sidenav();
      $(".dropdown-trigger").dropdown();
    </script>

<?php
  $entradaBuscada = "";
    echo "<script type='text/javascript'>
     $(document).ready(function() {
       $('#buscarBt').click(function(){
             $.ajax({
               type: 'POST',
               url: 'buscar.php',
               data: {'titol': $('#buscar').val()},
               success: function(data)
               { 
                 if (data !== 'empty'){
                  window.location.replace('index.php');
                } else {
                  M.toast({html: 'No se han encontrado resultados', classes: 'rounded'});
                }
                 
               }           
           });
          
         });
         
       });


          
       </script>";

       if( $_SESSION['dato'] !== ""){
        if ($_SESSION['buscados'] !== ""){
          $entradaBuscada = $_SESSION['buscados'];
          if($entradaBuscada == "empty"){
            $entradaBuscada = " ";
          }
         } 
       } else {
        $entradaBuscada = " ";
       }

       
       
?>
    
  </head>

  <style>
 body {
     display: flex;
     min-height: 100vh;
     flex-direction: column;
 }
 main {
     flex: 1 0 auto;
 }
 #login {
  background-color: white;
  border-radius: 10px;
}
#login_title {
  background-color: #4e342e !important;
  margin-top: 0px;
}
 </style>
<body class="orange accent-2">
<div class="navbar-fixed">
    <nav class="brown darken-4">
      <div class="nav-wrapper">
      <a href="index.php" class="brand-logo" style="margin-left: 12%" id="logo">E-Story</a>
        <a href="#" class="sidenav-trigger" data-target="responsive-nav">
          <i class="material-icons">menu</i>
        </a>
        <ul class="right hide-on-med-and-down">
          <li>
              <input type="text" id="buscar" class="autocomplete" style="background-color: white; " name="query">      
          </li>
          <li><a class="brown darken-2" style="color:white;" id="buscarBt">Buscar</a></li>
          <ul id="dropdown1" class="dropdown-content">
          <?php if (!empty($user)): ?>
          <li><a href="new_post.php" class="brown darken-2" style="color:white;">Publicar</a></li>
          <li><a href="perfil.php" class="brown darken-2" style="color:white;">Perfil</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <li><a href="#login" class="modal-trigger brown darken-2" style="color:white;">Entrar</a></li>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
          </ul>
          <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Acciones<i class="material-icons right">arrow_drop_down</i></a></li>
          
      </div>
    </nav>
    </div>

  <main>
  <div id="cuerpo">   
    <br>
    <br>

      <div class="row container">
      <div class="card-panel teal white">
        <h3 class="center-align">EDITA TU ENTRADA</h3>       
        <?php
            if (!empty($publicacion)) {
                    print("
                    <form method='POST' idea>
                        <div class='row'>
                        <div class='input-field col s12'>
                            <input name='title' type='text' placeholder='Título *' class='validate' required>
                            <span class='helper-text'></span>
                        </div>         
                        <div class='input-field col s12'>
                            <textarea name='texto' data-length='200'></textarea>
                        </div>
                        <div class='input-field col s12'>
                            <input name='resumen' type='text' placeholder='Resumen *' class='validate' required>
                            <span class='helper-text'></span>
                        </div>  
                        <div class='input-field col s12'>          
                            <input type='submit' class='btn btn-large brown darken-3' value='Submit'>
                        </div>
                        </div>
                    </form>
                          ");
            }
        ?>
        </div>
      </div>
   </div>
   </main>

   <div id="login" class="modal card">
        <h5 class="modal-close"></h5>
        <div class="card-action teal lighten-1 white-text" id="login_title">
          <h3 class="center">Logueate</h3>
        </div>
        <div class="modal-content center">
          <br>
          <form action="index.php" id="login" method="post">
            <div class="input-field">
              <i class="material-icons prefix">person</i>
              <input type="text" id="correo" name="email">
              <label for="name">Correo</label>
            </div>
            <br>
            <div class="input-field">
              <i class="material-icons prefix">lock</i>
              <input type="password" id="pass" name="password">
              <label for="pass">Contraseña</label>
            </div>
            <br>
            <div class="">
              <a href="signup.php"><h4>Registrate</h4></a>
            </div>
            <input type="submit" value="Entra" class="btn btn-large brown darken-3">
          </form>
        </div>
      </div>

   <footer class="page-footer brown darken-4 fixed">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">¿Qué es E-Story?</h5>
                <p class="grey-text text-lighten-4">
                E-Story, nace de la necesidad de tener un sitio en el que poder compartir con otras personas opiniones, información y más cosas sobre nuestra pasión común, la historia.
                </p>
                <p class="grey-text text-lighten-4">
                Aquí podrás encontrar todo lo que necesitas, además de satisfacer tu curiosidad o ayudarte en cualquier proyecto que necesite de información, a la vez que discutes
                con otros usuarios. 
                </p>             
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Efemérides, ¿Qué pasó hoy?</h5>
                <br>
                <?php

                $efem = $conn->prepare('SELECT * FROM efemerides WHERE dia = '.date('d').' AND mes = '.date('m').'');
                $efem->execute();            
                $resultadoEfem = $efem->fetch(PDO::FETCH_ASSOC);
                print($resultadoEfem['texto']);?>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © Copyright 2018-2019 Erik Cañizares
            <a class="grey-text text-lighten-4 right" href="signup.php"><b>¡Regístrate en menos de 5 minutos!</b></a>
            </div>
          </div>
        </footer>
      
        <ul class="sidenav" id="responsive-nav">
    <?php if (!empty($user)): ?>
    <li><a href="new_post.php" class="brown darken-2" style="color:white;">Publicar</a></li>
          <li><a href="perfil.php" class="brown darken-2" style="color:white;">Perfil</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <li><a href="#login" class="modal-trigger brown darken-2" style="color:white;">Entrar</a></li>
          <li><a href="signup.php" class="brown darken-2" style="color:white;">Registrate</a></li>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
      </ul>
    
</body>
</html>