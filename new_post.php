<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: /Projecte/projecte-final-de-grau");
}

require 'database.php';
$message ="";

if (!empty($_POST['editor'])) {
    $id = $_SESSION['user_id'];
    $sql = "INSERT INTO publicaciones (titulo, texto, resumen, categoria, usuario, fecha, pais, any) VALUES (:titulo, :texto, :resumen, :categoria, :usuario, :fecha, :pais, :any)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':titulo', $_POST['title']);
    $stmt->bindParam(':texto', $_POST['editor']);
    $stmt->bindParam(':resumen', $_POST['resumen']);
    $stmt->bindParam(':categoria', $_POST['categorias']);
    $stmt->bindParam(':usuario', $id);
    $date = date("Y-m-d H:i:s");
    $stmt->bindParam(':fecha', $date);
    $stmt->bindParam(':pais', $_POST['pais']);
    $stmt->bindParam(':any', $_POST['any']);

    if ($stmt->execute()) {
        header("Location: /Projecte/projecte-final-de-grau");
        $message = 'Successfully';
    } else {
      var_dump(msgfmt_get_error_message);
        $message = 'Sorry there must have been an issue creating your account';
    }
}

if (isset($_SESSION['user_id'])) {
  $id = $_SESSION['user_id'];
  $records = $conn->prepare('SELECT * FROM users WHERE id = :id');
  $records->bindParam(':id', $_SESSION['user_id']);
  $records->execute();
  $results = $records->fetch(PDO::FETCH_ASSOC);
  $user = null;

  if (!empty($results)) {
      $user = $results;
  }

  $recordsCount = $conn->prepare("SELECT COUNT(id_publicacion) FROM publicaciones WHERE usuario = $id");
  $recordsCount->execute();
  $resultadoCount = $recordsCount->fetch(PDO::FETCH_ASSOC);
  $count = null;

  if (!empty($resultadoCount)) {
      $count = $resultadoCount;
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

    <script type="text/javascript">
      //$(function() { $('textarea').froalaEditor() });
      $(document).ready(function(){
    $('select').formSelect();
  });
  
    </script>

<?php
  $entradaBuscada = "";
    echo "<script type='text/javascript'>
     $(document).ready(function() {
      $('.modal').modal();
      $('.sidenav').sidenav();
      $('.dropdown-trigger').dropdown();
       $('#buscarBt').click(function(){
             $.ajax({
               type: 'POST',
               url: 'buscar.php',
               data: {'titol': $('#buscar').val()},
               success: function(data)
               { 
                 if (data !== 'empty'){
                  window.location.replace('index.php');
                } else {
                  M.toast({html: 'No se han encontrado resultados', classes: 'rounded'});
                }
                 
               }           
           });
          
         });
         
       });


          
       </script>";

       if( $_SESSION['dato'] !== ""){
        if ($_SESSION['buscados'] !== ""){
          $entradaBuscada = $_SESSION['buscados'];
          if($entradaBuscada == "empty"){
            $entradaBuscada = " ";
          }
         } 
       } else {
        $entradaBuscada = " ";
       }

       
       
?>
    
    
  </head>

  <style>
 body {
     display: flex;
     min-height: 100vh;
     flex-direction: column;
 }
 main {
     flex: 1 0 auto;
 }
 </style>

<body class="orange accent-2">
<div class="navbar-fixed">
<nav class="brown darken-4">
      <div class="nav-wrapper">
      <a href="index.php" class="brand-logo" style="margin-left: 12%" id="logo">E-Story</a>
        <a href="#" class="sidenav-trigger" data-target="responsive-nav">
          <i class="material-icons">menu</i>
        </a>
        <ul class="right hide-on-med-and-down">
          <li>
              <input type="text" id="buscar" class="autocomplete" style="background-color: white; " name="query">      
          </li>
          <li><a class="brown darken-2" style="color:white;" id="buscarBt">Buscar</a></li>
          <ul id="dropdown1" class="dropdown-content">
          <?php if (!empty($user)): ?>
          <li><a href="perfil.php" class="brown darken-2" style="color:white;">Perfil</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
          </ul>
          <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Acciones<i class="material-icons right">arrow_drop_down</i></a></li>
          
      </div>
    </nav>
  </div>

  <main>
    <div id="cuerpo">   
    <br>
      <div class="row container">
      <div class="card-panel teal white">
        <h3 class="center-align">CREA TU POST</h3>       
        <blockquote class="">
          Rellena todos los campos con (*).
        </blockquote>
          <form action="new_post.php" method="POST" idea>
            <div class="row">
              <div class="input-field col s12">
                <input name="title" type="text" placeholder="Título *" class="validate" required>
                <span class="helper-text"></span>
              </div>      
              <div class="input-field col s12">
              <select name="categorias" required>
                <option value="" disabled selected>Categorías</option>
                <option value="prehistoria">Prehistoria</option>
                <option value="edad_antigua">Edad Antigua</option>
                <option value="edad_media">Edad Media</option>
                <option value="edad_moderna">Edad Moderna</option>
                <option value="edad_contemporanea">Edad Contemporanea</option>
              </select>
              <label>Escoge la categoría</label>
            </div>   
              <div class="input-field col s12">
                <textarea name="editor" data-length="200"></textarea>
              </div>
              <div class="input-field col s12">
                <input name="resumen" type="text" placeholder="Descripción *" class="validate" required>
                <span class="helper-text"></span>
              </div>
              <div class="input-field col s12">
                <input name="pais" type="text" placeholder="País">
                <span class="helper-text"></span>
              </div>  
              <div class="input-field col s12">
                <input name="any" type="text" placeholder="Año">
                <span class="helper-text"></span>
              </div>    
              <div class="input-field col s12">          
                <input type="submit" class="btn btn-large brown darken-3" value="Submit">
              </div>
            </div>
          </form>
        </div>
      </div>
   </div>
   </main>

   <footer class="page-footer brown darken-4 fixed">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">¿Qué es E-Story?</h5>
                <p class="grey-text text-lighten-4">
                E-Story, nace de la necesidad de tener un sitio en el que poder compartir con otras personas opiniones, información y más cosas sobre nuestra pasión común, la historia.
                </p>
                <p class="grey-text text-lighten-4">
                Aquí podrás encontrar todo lo que necesitas, además de satisfacer tu curiosidad o ayudarte en cualquier proyecto que necesite de información, a la vez que discutes
                con otros usuarios. 
                </p>             
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Efemérides, ¿Qué pasó hoy?</h5>
                <br>
                <?php

                $efem = $conn->prepare('SELECT * FROM efemerides WHERE dia = '.date('d').' AND mes = '.date('m').'');
                $efem->execute();            
                $resultadoEfem = $efem->fetch(PDO::FETCH_ASSOC);
                print($resultadoEfem['texto']);?>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © Copyright 2018-2019 Erik Cañizares
            <a class="grey-text text-lighten-4 right" href="signup.php"><b>¡Regístrate en menos de 5 minutos!</b></a>
            </div>
          </div>
        </footer>

        <ul class="sidenav" id="responsive-nav">
    <?php if (!empty($user)): ?>
    <li><a href="perfil.php" class="brown darken-2" style="color:white;">Perfil</a></li>
          <li><a href="logout.php" class="brown darken-2" style="color:white;">Desconectarse</a></li>
            <?php if ($user['administrador'] == 1): ?>
            <li><a href="admin.php" class="brown darken-2" style="color:white;">Administración</a></li>
            <?php endif;?>
          <?php else: ?>
          <?php endif;?>
          <li><a href="categorias.php" class="brown darken-2" style="color:white;">Categorias</a></li>
      </ul>
      
    
</body>
</html>