<?php

session_start();

require 'database.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: /Projecte/projecte-final-de-grau");
} else {
    $id = $_SESSION['user_id'];
    $records = $conn->prepare('SELECT administrador FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $admin = null;

    if ($results['administrador'] == 0) {
        header("Location: /Projecte/projecte-final-de-grau");
    } else {

        $records2 = $conn->prepare('SELECT * FROM users WHERE id != :id');
        $records2->bindParam(':id', $_SESSION['user_id']);
        $records2->execute();
        $results2 = $records2->fetchAll();
        $users = null;
        if (!empty($results2)){
            $users = $results2;
        }

        $records3 = $conn->prepare('SELECT * FROM publicaciones');
        $records3->execute();
        $results3 = $records3->fetchAll();
        $publ = null;
            if (!empty($results3)){
                $publ = $results3;
                }


    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/admin/styles.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <title>Document</title>
    <script>
    $(document).ready(function() {

        $( "#usersDiv" ).show();
        $( "#publDiv" ).hide();

        $('#tabla').DataTable( {
        });

        $('#tabla2').DataTable( {
            responsive: true,
        });

        $( "#users" ).click(function() {
            $( "#usersDiv" ).show();
            $( "#publDiv" ).hide();
        });

        $( "#publicaciones" ).click(function() {
            $( "#publDiv" ).show();
            $( "#usersDiv" ).hide();
            
        });

        $( ".btn btn-success" ).click(function() {
            alert("Hi");            
        });

    } );
    </script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Administración</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.php">Volver</a>
      <a class="nav-item nav-link active" href="#" id="users">Users</a>
      <a class="nav-item nav-link active" href="#" id="publicaciones">Publicaciones</a>
    </div>
  </div>
</nav>

<br>


<div id="cuerpo" class="container">
<div class="row">
    <div class="col s12" id="usersDiv">
        <table id="tabla" class="mdl-data-table centerTable" style="width:100%" >
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User</th>
                    <th>Email</th>
                    <th>Dirección</th>
                    <th>Aniversario</th>
                    <th>Es administrador?</th>
                    <th>Eliminar Usuario</th>
                    <th>Administrar privilegios</th>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                    if(!empty($users)){
                        $isAdmin = "";
                    foreach ($users as $usuari){
                        if($usuari['administrador'] == 1) {
                            $isAdmin = "Sí";
                        } else {
                            $isAdmin = "No";
                        }
                        print("
                        <tr>
                            <td>".$usuari['id']."</td>
                            <td>".$usuari['user']."</td>
                            <td>".$usuari['email']."</td>
                            <td>".$usuari['direccion']."</td>
                            <td>".$usuari['aniversario']."</td>
                            <td>".$isAdmin."</td>
                            <td><form method='post' action='eliminarUser.php?id=".$usuari['id']."'><button type='submit' class='btn btn-danger' id='deleteUser'>DELETE</button></form></td>
                            <td><form method='post' action='adminUser.php?id=".$usuari['id']."'><button type='submit' class='btn btn-success id='adminUser'>ADMIN</button></form></td>
                        </tr>"
                    );
                    }
                    }          
                ?>
            </tbody>
        </table>
    </div>
    <div class="col 12" id="publDiv">
        <table id="tabla2" class="mdl-data-table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>User</th>
                    <th>Fecha</th>
                    <th>Eliminar Publicación</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if(!empty($publ)){
                    foreach ($publ as $publicacion){
                        print("
                        <tr>
                            <td>".$publicacion['id_publicacion']."</td>
                            <td>".$publicacion['titulo']."</td>
                            <td>".$publicacion['usuario']."</td>
                            <td>".$publicacion['fecha']."</td>
                            <td><form method='post' action='eliminarPubl.php?id=".$publicacion['id_publicacion']."'><button type='submit' class='btn btn-danger' id='deletePubl'>DELETE</button></form></td>
                        </tr>"
                    );
                    }
                    }          
                ?>
            </tbody>
        </table>
    </div>
    </div>
</div>
                  

</body>
</html>
