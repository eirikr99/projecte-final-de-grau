<?php 

session_start();

require 'database.php';

$id_user = $_GET['id'];


if (!isset($_SESSION['user_id'])) {
    header("Location: /Projecte/projecte-final-de-grau");
} else {
    $id = $_SESSION['user_id'];
    $records = $conn->prepare('SELECT administrador FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    if ($results['administrador'] == 0) {
        header("Location: /Projecte/projecte-final-de-grau");
    } else {
        $records2 = $conn->prepare('DELETE FROM users WHERE id = '.$id_user.'');
        $records2->execute();
        header("Location: admin.php");
    }
}
?>